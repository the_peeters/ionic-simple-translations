import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'ionicTranslations',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;

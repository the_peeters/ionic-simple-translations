import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';


//translations 

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { IonicStorageModule } from '@ionic/storage-angular';

export function createTranslateLoader(http: HttpClient) {
	return new TranslateHttpLoader(http, 'assets/i18n/', '.json');
}

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule, 
    IonicModule.forRoot(), 
    AppRoutingModule,
    HttpClientModule,
		IonicStorageModule.forRoot(),
		TranslateModule.forRoot({
			loader: {
				provide: TranslateLoader,
				useFactory: (createTranslateLoader),
				deps: [HttpClient]
			}
		}),
  
  ],
  providers: [
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}

/*
=#=#=#=#=#=#= TRNSLATIONS IONIC 5 =#=#=#=#=#=#=
src: https://github.com/AndrewJBateman/ionic-angular-localiser


======== package.json ======== Important this otherwise wont complie
"dependencies": {
  ....
    "@ngx-translate/core": "^13.0.0",
    "@ngx-translate/http-loader": "^6.0.0",
  }
$ npm install (to intall dependencies)

$ npm install @ionic/storage
======== app.modules.ts ========
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { IonicStorageModule } from '@ionic/storage-angular';

export function createTranslateLoader(http: HttpClient) {
	return new TranslateHttpLoader(http, 'assets/i18n/', '.json');
}
@NgModule({
  
  imports: [
    
    HttpClientModule,
		IonicStorageModule.forRoot(),
		TranslateModule.forRoot({
			loader: {
				provide: TranslateLoader,
				useFactory: (createTranslateLoader),
				deps: [HttpClient]
			}
		}),
  
  ],
})
======== app.component.ts ========
import { TranslateService } from '@ngx-translate/core';
export class AppComponent {
  constructor( private translate: TranslateService) {  
    this.translate.setDefaultLang('en'); 
  }
}
======== XY(page).modules.ts ========
import { XYPage } from './XY.page'; 


import { RouterModule } from "@angular/router";
import { TranslateModule } from "@ngx-translate/core";

@NgModule({
  imports: [
    TranslateModule,
    RouterModule.forChild([
			{
				path: "",
				component: XYPage,
			},
		]),
  ],
  declarations: [XYPage]
})

===========================================
translations /src/assets/i18n/LANGCODE.json
-------- language json format --------
{
  "SECTION": {
    "ITEM": "Tab One in english"
  }
}
-------- HTML --------

{{ 'SECTION.ITEM' | translate: params }}
<ion-text [translate]="'SECTION.ITEM'" [translateParams]="params" ></ion-text>

=\=/=\=\=/=\=\=/=\=\=/=\=\=/=\=\=/=\=\=/=\=\=/=\
EXTENSION

ionic generate service LanguageConfig --skipTests=true
---- services/lang/language-condig.ts -----
import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

export class LanguageConfigService {

  constructor(private translate: TranslateService) { }
  getDefaultLanguage():string{
    let language = this.translate.getBrowserLang();
    this.translate.setDefaultLang(language);
    return language;
  }

  setLanguage(selectedLanguage: string) {
    this.translate.use(selectedLanguage);
  }
}
-------------  XY(page),page.ts -------------
import { Component } from '@angular/core';
import { LanguageConfigService } from '../../';

export class XYPage {

  selectedLanguage:string;

  constructor(private translateConfigService: LanguageConfigService){
    this.selectedLanguage = this.translateConfigService.getDefaultLanguage();
  }

  languageChanged(){
    this.translateConfigService.setLanguage(this.selectedLanguage);
  }

}


*/
import { Component, OnInit } from '@angular/core';
import {PopoverController} from '@ionic/angular';

@Component({
  selector: 'app-lang-selector',
  templateUrl: './lang-selector.component.html',
  styleUrls: ['./lang-selector.component.scss'],
})
export class LangSelectorComponent implements OnInit {

  constructor(private popover: PopoverController) { }

  ngOnInit() {}

  closePopover(){
    this.popover.dismiss();
  }


}

import { Component } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { LanguageConfigService } from '../services/lang/language-config.service';
import { LangSelectorComponent } from '../components/popover/lang-selector/lang-selector.component';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  params = {
		name: "Batman",
	};

  selectedLanguage:string;

  constructor(
    private translateConfigService: LanguageConfigService,
    private popover: PopoverController
    ){
    this.selectedLanguage = this.translateConfigService.getDefaultLanguage();
  }

  languageChanged(){
    console.log(this.selectedLanguage);
    
    this.translateConfigService.setLanguage(this.selectedLanguage);
  }
  createPopover(){
    this.popover.create({component:LangSelectorComponent,showBackdrop: false}).then(popoverElement=>{
      popoverElement.present();
    });
  }

}

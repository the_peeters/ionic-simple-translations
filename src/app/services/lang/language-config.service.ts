import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class LanguageConfigService {

  constructor(private translate: TranslateService) { }
  getDefaultLanguage():string{
    let language = this.translate.getBrowserLang();
    console.log("default language=>",language);
    
    this.translate.setDefaultLang(language);
    return language;
  }

  setLanguage(selectedLanguage: string) {
    this.translate.use(selectedLanguage);
  }
}
